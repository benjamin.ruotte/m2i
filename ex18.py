def check_item(memo, find_dnas):
    i = 1
    while len(memo) > i:
        y = 0
        test = memo[i] - memo[i-1]
        while y < len(find_dnas):
            if test < len(find_dnas[y]):
                return False
            y = y + 1
        i = i + 1
    return True


def main():
    suspect = [["Colonel Moutarde", "CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGCADN"],
               ["Mlle Rose", "CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGGADN"],
               ["Mme Pervenche", "AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCCADN"],
               ["M Leblanc", "CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"]]
    find_dnas = ["CATA", "ATGC"]
    i = 0
    check = 0
    memo = []
    while len(suspect) > i | check < len(find_dnas):
        y = 0
        check = 0
        while len(find_dnas) > y | check < len(find_dnas):
            if suspect[i][1].find(find_dnas[y]) > 0:
                check = check + 1
                memo.append(suspect[i][1].find(find_dnas[y]))
            y = y + 1
        if check_item(memo, find_dnas) is not False:
            if check >= len(find_dnas):
                break
        i = i + 1

    print("le coupable est "+ suspect[i][0])


if __name__ == "__main__":
    main()
