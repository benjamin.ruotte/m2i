def palindrome(fWord):
    i = len(fWord)-1
    for char in fWord:
        if fWord[i] != char:
            return False
        i = i - 1
    return True

def main():
    fWord = input("enter un mot : ")
    if palindrome(fWord.upper()) is True:
        print("le mot "+ fWord + " est un palindrome")
    else:
        print("le mot "+ fWord + " n'est pas un palindrome")

if __name__ == "__main__":
    main()
