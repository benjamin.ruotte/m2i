def distance_hamming(fWord, sWord):
    i = 0
    diff = 0
    print(sWord)
    for char in fWord:
        if sWord[i] != char:
            diff = diff + 1
        i = i + 1
    print("la distance entre les deux mots est de " + str(diff))

def main():
    fWord = input("enter le premier mot : ")
    sWord = input("enter le second mot : ")
    if len(fWord) == len(sWord):
        distance_hamming(fWord.upper(), sWord.upper())
    else:
        print("veuillez entrer des mots de même taille")

if __name__ == "__main__":
    main()
